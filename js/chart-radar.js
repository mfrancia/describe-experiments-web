function drawRadar(prop, data, d1, M) {
    // set the dimensions and margins of the graph
    var margin = prop.margin, width = prop.width, height = prop.height;

    var miny = 0, maxy = 0;
    var newdata = {};
    var domain = 0;
    data.forEach(function(d) {
        domain++;
        M.forEach(function (m) {
            miny = Math.min(miny, d[m]);
            maxy = Math.max(maxy, d[m]);
            datum = newdata[m] || {};
            datum[d[d1]] = d; 
            newdata[m] = datum;
        })
    });
    data = newdata;

    // append the svg object to the body of the page
    var div = d3.select("#my_dataviz").append("div");
    var svg = div
        .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
        .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    let radialScale = d3.scaleLinear()
                        .domain([miny,maxy])
                        .range([0, height/2]);
    let ticks = [miny, maxy / 4, maxy * 2 / 4, maxy * 3 / 4, maxy];

    ticks.forEach(t =>
        svg.append("circle")
            .attr("cx", height / 2)
            .attr("cy", height / 2)
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("r", radialScale(t))
    );
    
    // ticks.forEach(t =>
    //     svg.append("text")
    //         .attr("x", (height - 40) / 2)
    //         .attr("y", (height - 40) / 2 - radialScale(t))
    //         .text(Math.round(t).toString())
    // );
    
    function angleToCoordinate(angle, value){
        let x = Math.cos(angle) * radialScale(value);
        let y = Math.sin(angle) * radialScale(value);
        return {"x": (height / 2) + x, "y": (height / 2) - y};
    }

    var i = 0;
    for (const [member, d] of Object.entries(data[M[0]])) {
        let angle = (Math.PI / 2) + (2 * Math.PI * i++ / domain);
        let line_coordinate = angleToCoordinate(angle, maxy);
        let label_coordinate = angleToCoordinate(angle, maxy + maxy * 0.05);
    
        //draw axis line
        svg.append("line")
            .attr("x1", (height / 2))
            .attr("y1", (height / 2))
            .attr("x2", line_coordinate.x)
            .attr("y2", line_coordinate.y)
            .attr("stroke","black");
    
        //draw axis label
        svg.append("text")
            .attr("x", label_coordinate.x)
            .attr("y", label_coordinate.y)
            .text(member)
                .attr("text-anchor", function(d) { if (label_coordinate.x < height / 2) return "end"; else return "begin" })
    }
    
    let line = d3.line()
                 .x(d => d.x)
                 .y(d => d.y);

    function getPathCoordinates(measure, datum) {
        let coordinates = [];
        var i = 0;
        for (const [member, d] of Object.entries(datum)) {
            let angle = (Math.PI / 2) + (2 * Math.PI * i++ / domain);
            coordinates.push(angleToCoordinate(angle, d[measure]));
        };
        // for (var i = 0; i < data[d].length; i++) {
        //     let ft_name = data[i];
        //     let angle = (Math.PI / 2) + (2 * Math.PI * i / data.length);
        //     coordinates.push(angleToCoordinate(angle, data_point[ft_name]));
        // }
        return coordinates;
    }
    
    // Add a scale for bubble color
    var lineColor = d3.scaleOrdinal().domain(M).range(greyPalette());
    var tooltip = createTooltip(div);

    i = 0;
    for (const [measure, level] of Object.entries(data)) {
        // sleep(100).then(() => {
            console.log(level);
            let color = lineColor[i++];
            let coordinates = getPathCoordinates(measure, level);
            //draw the path element
            svg.append("path")
                .datum(coordinates)
                // .attr("class", "line")
                // .style("stroke", color)
                // .attr("stroke", "cyan")
                // .attr("stroke-width", 2.5)
                // .attr("stroke-opacity", 1)
                .style("fill", color)
                // .attr("stroke", color)
                // .attr("stroke-width", 10)
                .style("fill-opacity", 0.2)
                .attr("d", line)
                
            svg.selectAll(".point")
                .data(Object.values(level))
                .enter()
                .append("circle")
                    .attr("class", "dot") // Assign a class for styling
                    .attr("cx", function(d, i) { return coordinates[i].x })
                    .attr("cy", function(d, i) { return coordinates[i].y })
                    .attr("datapoint", "colored")
                    .attr("r", 4)
                    .on("mouseover",  function f (d) { showTooltip(d, tooltip) })
                    .on("mousemove",  function f (d) { moveTooltip(d, tooltip) })
                    .on("mouseleave", function f (d) { hideTooltip(d, tooltip) });
        // });
    };
    appendLegend(svg, M, width, height, lineColor);
}